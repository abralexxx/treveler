<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class RateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('rating')->insert([
            'place_id' => 1,
            'user_id' => 1,
            'rate' => random_int(1, 5),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
